﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IngameScript.Configuration
{
    /**
     * Definition for configuration where the section name is not important
     */
    public class InnerConfigDefinition<Type>
    {
        public List<ConfigKeyFieldDefinition<Type>> fields;
        public Func<Type> instanceCreator;
    }

    /**
     * Definition for configuration where the section name is part of the configuration
     */
    public class SectionConfigDefinition<Type> : InnerConfigDefinition<Type>
    {
        public ConfigSectionFieldDefinition<Type> sectionField;
    }

    public abstract class ConfigFieldDefinition<Type>
    {
        public Action<Type, object> fieldSetter;
    }

    public class ConfigKeyFieldDefinition<Type> : ConfigFieldDefinition<Type>
    {
        public string keyName;
        public ConfigFieldType type;
        internal object defaultValue;
    }

    public class ConfigSectionFieldDefinition<Type> : ConfigFieldDefinition<Type>
    {
        public string prefix;
        public string suffix;
    }
}
