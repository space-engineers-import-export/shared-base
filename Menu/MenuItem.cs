﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IngameScript.Menu
{
    abstract class MenuItem
    {
        public readonly string name;
        internal MenuFolder parent;

        protected MenuItem(string name)
        {
            this.name = name;
        }
    }

    class MenuFolder : MenuItem
    {
        readonly List<MenuItem> menuItems = new List<MenuItem>();

        public int maxIndex
        {
            get { return menuItems.Count - 1; }
        }

        public MenuFolder(string name) : base(name)
        { }

        public void addMenuItem(MenuItem item)
        {
            if (item.parent != null)
            {
                throw new ArgumentException("tried to add Menu Item " + item.name + " multiple times");
            }
            item.parent = this;
            menuItems.Add(item);
        }

        public MenuItem getMenuItem(int index)
        {
            if (menuItems.Count == 0)
            {
                return null;
            }
            else
            {
                return menuItems[index];
            }
        }
    }

    class MenuAction : MenuItem
    {
        readonly Action onTrigger;

        public MenuAction(string name, Action onTrigger) : base(name)
        {
            this.onTrigger = onTrigger;
        }

        public void trigger()
        {
            onTrigger.Invoke();
        }
    }
}
