﻿using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRageMath;

namespace IngameScript.Configuration
{
    public abstract class ConfigFieldTypes
    {
        public static readonly BoolField BOOL = new BoolField();
        public static readonly IntegerField INTEGER = new IntegerField();
        public static readonly DoubleField DOUBLE = new DoubleField();
        public static readonly FloatField FLOAT = new FloatField();
        public static readonly StringField STRING = new StringField();
        public static readonly WaypointField WAYPOINT = new WaypointField();
        public static readonly ColorField COLOR = new ColorField();
        public static readonly WaypointListField WAYPOINT_LIST = new WaypointListField();
        public static readonly StringListField STRING_LIST = new StringListField();
    }

    public abstract class ConfigFieldType
    {
        public object getValueFromConfig(MyIni ini, string section, string key, object defaultValueProvider)
        {
            if (!ini.ContainsKey(section, key))
            {
                return defaultValueProvider;
            }
            return getValue(ini.Get(section, key));
        }

        abstract public object getValue(MyIniValue iniValue);
    }

    public class BoolField : ConfigFieldType
    {
        public override object getValue(MyIniValue iniValue)
        {
            return iniValue.ToBoolean();
        }
    }

    public class IntegerField : ConfigFieldType
    {
        public override object getValue(MyIniValue iniValue)
        {
            return iniValue.ToInt32();
        }
    }

    public class DoubleField : ConfigFieldType
    {
        public override object getValue(MyIniValue iniValue)
        {
            return iniValue.ToDouble();
        }
    }

    public class FloatField : ConfigFieldType
    {
        public override object getValue(MyIniValue iniValue)
        {
            return iniValue.ToSingle();
        }
    }

    public class StringField : ConfigFieldType
    {
        public override object getValue(MyIniValue iniValue)
        {
            return iniValue.ToString();
        }
    }

    public class WaypointField : ConfigFieldType
    {
        public override object getValue(MyIniValue iniValue)
        {
            MyWaypointInfo gps;
            if (!MyWaypointInfo.TryParse(iniValue.ToString(), out gps))
            {
                throw new ArgumentException("can not parse '" + iniValue.ToString() + "' to GPS coordinates");
            }
            return gps;
        }
    }

    public class ColorField : ConfigFieldType
    {

        private static readonly System.Text.RegularExpressions.Regex COLOR_REGEX = new System.Text.RegularExpressions.Regex("[a-fA-F0-9]{6,8}");

        public override object getValue(MyIniValue iniValue)
        {
            var colorString = iniValue.ToString().Replace("#", "");
               if (!COLOR_REGEX.IsMatch(colorString))
                {
                    throw new ArgumentException("can not parse '" + colorString + "' to Color");
              }
            int red;
            int green;
            int blue;
            int alpha = 255;
            int.TryParse("0x" + colorString.Substring(0, 2), out red);
            int.TryParse("0x" + colorString.Substring(2, 2), out green);
            int.TryParse("0x" + colorString.Substring(4, 2), out blue);
            if (colorString.Length == 8)
                int.TryParse("0x" + colorString.Substring(6, 2), out alpha);

            return new Color(red, green, blue, alpha);
        }
    }

    public abstract class ListField<Type> : ConfigFieldType where Type : ConfigFieldType
    {
        protected Type type;
        protected ListField(Type type)
        {
            this.type = type;
        }

        public override object getValue(MyIniValue iniValue)
        {
            var lines = new List<string>();
            iniValue.GetLines(lines);

            var values = new List<object>();
            MyIniKey fakeKey = new MyIniKey("foo","bar");
            foreach (var line in lines)
            {
                MyIniValue fakeIniValue = new MyIniValue(fakeKey, line);

                values.Add(type.getValue(fakeIniValue));
            }
            return values;
        }
    };

    public class WaypointListField : ListField< WaypointField>
    {
        public WaypointListField() : base(ConfigFieldTypes.WAYPOINT) { }
    }

    public class StringListField : ListField< StringField>
    {
        public StringListField() : base(ConfigFieldTypes.STRING) { }

        public override object getValue(MyIniValue iniValue)
        {
            var lines = new List<string>();
            iniValue.GetLines(lines);

            return lines;
        }
    }
}
