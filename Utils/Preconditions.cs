﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IngameScript.Utils
{
    class Preconditions
    {
        public static Type checkNotNull<Type>(Type obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException();
            }
            return obj;
        }

        public static string checkNotNullOrEmpty(string str)
        {
            if (str == null)
            {
                throw new ArgumentNullException();
            }
            if (str.Length == 0)
            {
                throw new ArgumentException("string may not be empty");
            }
            return str;
        }
    }
}
