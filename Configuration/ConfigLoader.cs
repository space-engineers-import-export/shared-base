﻿using System;
using System.Collections.Generic;
using VRage.Game.ModAPI.Ingame.Utilities;

namespace IngameScript.Configuration
{
    abstract class ConfigLoader
    {

        public static Type load<Type>(InnerConfigDefinition<Type> definition, MyIni ini, string section, Action<string> Echo)
        {
            Type instance = definition.instanceCreator();
            foreach (var field in definition.fields)
            {
                object defaultValue = field.defaultValue;
                var value = field.type.getValueFromConfig(ini, section, field.keyName, defaultValue);

                field.fieldSetter(instance, value);
            }

            return instance;
        }

        public static List<Type> load<Type>(SectionConfigDefinition<Type> definition, MyIni ini, Action<string> Echo)
        {
            List<Type> instances = new List<Type>();

            var sections = new List<string>();
            ini.GetSections(sections);

            foreach (var section in sections)
            {
                if (section.StartsWith(definition.sectionField.prefix) && section.EndsWith(definition.sectionField.suffix))
                {
                    var name = section.Substring(definition.sectionField.prefix.Length, section.Length - definition.sectionField.prefix.Length - definition.sectionField.suffix.Length).Trim();

                    Type instance = load(definition, ini, section, Echo);
                    definition.sectionField.fieldSetter(instance, name);
                    instances.Add(instance);
                }
            }

            return instances;
        }
    }
}
