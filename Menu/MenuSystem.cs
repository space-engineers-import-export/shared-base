﻿using IngameScript.Utils;
using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using System.Text;
using VRage.Game.GUI.TextPanel;

namespace IngameScript.Menu
{
    class MenuSystem
    {
        public const string DEFAULT_MENU_COMMAND = "menu";

        private readonly MenuFolder root;
        private readonly MenuCommandParser parser;
        private readonly List<IMyTextSurface> surfaces;
        private readonly MenuState state;
        private readonly bool closable;
        // for debug stuff
        private readonly Action<string> Echo;

        public MenuSystem(List<IMyTextSurface> surfaces,
            MenuFolder root,
            Action<String> Echo) : this(surfaces, root, DEFAULT_MENU_COMMAND, true, Echo) { }

        public MenuSystem(List<IMyTextSurface> surfaces,
            MenuFolder root,
            string menuCommand = DEFAULT_MENU_COMMAND,
            bool closable = true,
            Action<String> Echo = null)
        {
            this.surfaces = Preconditions.checkNotNull(surfaces);
            this.root = Preconditions.checkNotNull(root);
            this.parser = new MenuCommandParser(Preconditions.checkNotNullOrEmpty(menuCommand));
            this.closable = closable;
            if (Echo != null)
                this.Echo = Echo;
            else
                this.Echo = delegate (String ignored) { };
            state = new MenuState(root);
            if (!closable)
            {
                state.openClose(true);
            }
        }

        public void handleCommand(string commandStr)
        {
            var command = parser.parse(commandStr);
            if (!command.isValid)
            {
                // ignore commands not meant for the menu
                return;
            }

            switch (command.command)
            {
                case CommandType.CONFIRM:
                    state.trigger();
                    break;
                case CommandType.NEXT:
                    state.next();
                    break;
                case CommandType.PREVIOUS:
                    state.previous();
                    break;
                case CommandType.BACK:
                    state.back();
                    break;
                case CommandType.OPEN:
                    state.openClose(true);
                    break;
                case CommandType.CLOSE:
                    if (closable)
                        state.openClose(false);
                    break;
                case CommandType.OPEN_CLOSE:
                    if (!state.open || closable)
                        state.openClose(!state.open);
                    break;
            }
        }

        public void close()
        {
            if (closable)
            {
                state.openClose(false);
            }
        }

        public bool printMenu()
        {
            if (!state.open)
            {
                return false;
            }

            printTextMenu();
            printScriptMenu();

            return true;
        }

        private void printScriptMenu()
        {
            var drawFrames = new List<MySpriteDrawFrame>();
            foreach (var surface in surfaces)
            {
                if (surface.ContentType == ContentType.SCRIPT)
                {
                    drawFrames.Add(surface.DrawFrame());
                }
            }

            Action<MySprite> draw = delegate (MySprite sprite)
             {
                 foreach (var frame in drawFrames)
                 {

                     frame.Add(sprite);
                 }
             };

            // TODO

            foreach (var frame in drawFrames)
            {
                frame.Dispose();
            }
        }

        private void printTextMenu()
        {
            List<string> menuLines = new List<string>();
            for (int i = 0; i <= state.currentMenu.maxIndex; i++)
            {
                string menuText = "";
                if (i == state.currentSelectedItem) { menuText += "> "; }
                else { menuText += "  "; }
                menuText += state.currentMenu.getMenuItem(i).name;

                menuLines.Add(menuText + "\n");
            }

            foreach (var surface in surfaces)
            {
                if (surface.ContentType == ContentType.TEXT_AND_IMAGE)
                {
                    Echo(surface.DisplayName);
                    surface.WriteText("");
                    // 30 = font size in pixel
                    var lineSize = 30 * surface.FontSize;
                    var maxLines = (int)(surface.SurfaceSize.Y / lineSize);
                    var maxSkipLines = Math.Max(0, menuLines.Count - maxLines + 1);
                    var skipLines = Math.Min(maxSkipLines, Math.Max(0, state.currentSelectedItem - (maxLines / 2)));
                    var limit = Math.Min(skipLines + maxLines, menuLines.Count);

                    Echo("limit: " + limit);
                    Echo("skiplines: " + skipLines);
                    Echo("maxlines: " + maxLines);

                    for (int i = skipLines; i < limit; i++)
                    {
                        surface.WriteText(menuLines[i], true);
                    }
                }
            }
        }


        private enum CommandType
        {
            CONFIRM,
            NEXT,
            PREVIOUS,
            BACK,
            OPEN,
            CLOSE,
            OPEN_CLOSE
        }

        private class MenuState
        {
            public bool open = false;
            public MenuFolder currentMenu;
            public int currentSelectedItem = 0;
            private MenuFolder root;

            public MenuState(MenuFolder root)
            {
                this.currentMenu = root;
                this.root = root;
            }

            public void trigger()
            {
                var item = currentMenu.getMenuItem(currentSelectedItem);
                if (item != null)
                {
                    if (item is MenuAction)
                    {
                        ((MenuAction)item).trigger();
                    }
                    else if (item is MenuFolder)
                    {
                        currentMenu = (MenuFolder)item;
                        currentSelectedItem = 0;
                    }
                }
            }

            public void next()
            {
                if (currentSelectedItem < currentMenu.maxIndex)
                {
                    currentSelectedItem++;
                }
            }
            public void previous()
            {
                if (currentSelectedItem > 0)
                {
                    currentSelectedItem--;
                }
            }
            public void openClose(bool open)
            {
                this.open = open;
                currentMenu = root;
                currentSelectedItem = 0;
            }

            public void back()
            {
                if (currentMenu.parent != null)
                {
                    currentMenu = currentMenu.parent;
                    currentSelectedItem = 0;
                }
                else
                {
                    openClose(false);
                }
            }

        }

        private class MenuCommand
        {
            public readonly bool isValid;
            public readonly CommandType command;

            private MenuCommand()
            {
                isValid = false;
            }
            private MenuCommand(CommandType command)
            {
                this.isValid = true;
                this.command = command;
            }

            public static MenuCommand invalid()
            {
                return new MenuCommand();
            }

            public static MenuCommand of(CommandType command)
            {
                return new MenuCommand(command);
            }
        }

        private class MenuCommandParser
        {
            readonly string menuCommand;
            public MenuCommandParser(string menuCommand)
            {
                this.menuCommand = menuCommand;
            }

            public MenuCommand parse(string command)
            {
                var prefix = menuCommand + " ";
                if (!command.StartsWith(prefix))
                {
                    return MenuCommand.invalid();
                }

                var commandTypeString = command.Remove(0, prefix.Length);

                CommandType commandType;
                if (!Enum.TryParse(commandTypeString, true, out commandType))
                {
                    return MenuCommand.invalid();
                }

                return MenuCommand.of(commandType);
            }
        }
    }

}
