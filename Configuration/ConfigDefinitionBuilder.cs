﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IngameScript.Configuration.Builder
{
    public abstract class ConfigItemDefinitionBuilder
    {

        public static InnerConfigDefinitionBuilder<Type> ofInnerConfig<Type>(Func<Type> instanceCreator)
        {
            return new InnerConfigDefinitionBuilder<Type>(instanceCreator);
        }

        public static SectionConfigDefinitionBuilder<Type> ofSectionConfig<Type>(Func<Type> instanceCreator,
            string prefix,
            string suffix,
            Action<Type, object> fieldSetter)
        {
            return new SectionConfigDefinitionBuilder<Type>(instanceCreator).setFieldFromSection(prefix, suffix, fieldSetter);
        }
    }

    public abstract class DefinitionBuilderBase<Type, BuilderType, DefinitionType>
        where BuilderType : DefinitionBuilderBase<Type, BuilderType, DefinitionType>
        where DefinitionType : InnerConfigDefinition<Type>, new()
    {
        protected DefinitionType definition = new DefinitionType();

        protected DefinitionBuilderBase(Func<Type> instanceCreator)
        {
            definition.instanceCreator = instanceCreator;
            definition.fields = new List<ConfigKeyFieldDefinition<Type>>();
        }

        private ConfigKeyFieldDefinition<Type> createFieldDefinition<FieldType>(string keyname,
                FieldType type,
                Action<Type, object> fieldSetter) where FieldType : ConfigFieldType
        {
            var fieldDefinition = new ConfigKeyFieldDefinition<Type>();
            fieldDefinition.keyName = keyname;
            fieldDefinition.type = type;
            fieldDefinition.fieldSetter = fieldSetter;
            return fieldDefinition;
        }

        public BuilderType addField<FieldType>(string keyname,
                FieldType type,
                Action<Type, object> fieldSetter) where FieldType : ConfigFieldType
        {
            definition.fields.Add((ConfigKeyFieldDefinition<Type>)(object)createFieldDefinition<FieldType>(keyname, type, fieldSetter));
            return (BuilderType)this;
        }

        public BuilderType addField<FieldType, ValueType>(string keyname,
               FieldType type,
               ValueType defaultValue,
               Action<Type, object> fieldSetter) where FieldType : ConfigFieldType
        {
            var fieldDefinition = (ConfigKeyFieldDefinition<Type>)(object)createFieldDefinition<FieldType>(keyname, type, fieldSetter);
            fieldDefinition.defaultValue = defaultValue;
            definition.fields.Add((ConfigKeyFieldDefinition<Type>)(object)fieldDefinition);
            return (BuilderType)this;
        }

        public virtual DefinitionType build()
        {
            return definition;
        }
    }

    public class SectionConfigDefinitionBuilder<Type> : DefinitionBuilderBase<Type, SectionConfigDefinitionBuilder<Type>, SectionConfigDefinition<Type>>
    {
        public SectionConfigDefinitionBuilder(Func<Type> instanceCreator) : base(instanceCreator)
        {
        }

        internal SectionConfigDefinitionBuilder<Type> setFieldFromSection(string prefix,
                string suffix,
                Action<Type, object> fieldSetter)
        {
            if (definition.sectionField != null)
            {
                throw new InvalidOperationException("only one section field is allowed");
            }

            var fieldDefinition = new ConfigSectionFieldDefinition<Type>();
            fieldDefinition.prefix = prefix;
            fieldDefinition.suffix = suffix;
            fieldDefinition.fieldSetter = fieldSetter;
            definition.sectionField = (ConfigSectionFieldDefinition<Type>)(object)fieldDefinition;
            return this;
        }
    }

    public class InnerConfigDefinitionBuilder<Type> : DefinitionBuilderBase<Type, InnerConfigDefinitionBuilder<Type>, InnerConfigDefinition<Type>>
    {
        public InnerConfigDefinitionBuilder(Func<Type> instanceCreator) : base(instanceCreator)
        {
        }
    }
}
